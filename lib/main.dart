import 'package:flutter/material.dart'; //
// import 'package:helloworld/Layout/About.dart';
// import 'package:helloworld/About.dart';
import 'package:helloworld/Layout/LoginActivity/login_page.dart';
// import 'package:helloworld/QCECv3.dart';
//import 'package:helloworld/ScanTicketView.dart';
//import 'package:helloworld/ScanTicketView.dart';
//import 'package:helloworld/fxDriver.dart';
//import 'package:helloworld/fxDriver.dart';
//import 'package:helloworld/home_page.dart';

//import 'package:helloworld/login_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Staff app',
      theme: ThemeData(
        primarySwatch: Colors.teal,
        fontFamily: 'Raleway',
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginPage(),
    );
  }
}
