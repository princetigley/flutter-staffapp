import 'package:shared_preferences/shared_preferences.dart';
// import 'package:shared_preferences/shared_preferences.dart';

Future<String> fetchData() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String object = prefs.getString('dataresponse');
  return object;
}
