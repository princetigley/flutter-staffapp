class Strings {
  Strings._();
  //Error Messages
  static const String API_VALIDATION_ERROR = "Api validation";
  static const String STAFF_NOT_FOUND = "Staff not found!";
  static const String PASSWORD_MISMATCH = "Password Mismatch";
  static const String UPLOAD_ERROR = "Upload Error!";
  static const String UNKNOWN_ERROR = "Unknown Error!";
  static const String ADMIN_NO_ACCESS = "Admin no access!";
  static const String INVALID_FILE_TYPE = "Invalid file type!";
  static const String NO_USER_ACCESS = "No user access!";
  static const String SESSION_NOT_FOUND = "Session not found";
  static const String OTP_INVALID = "Invalid OTP";
  static const String OTP_TIMEDOUT = "OTP Timeout!";
  static const String BUS_NOT_ACTIVE = "Inactive Bus";
  static const String BUS_NOT_FOUND = "Bus not found!";
  static const String BUS_HAS_BEEN_DELETED = "Bus has been deleted";
  static const String CODE_NOT_FOUND = "Code not found";

  static const String TICKET_NOT_FOUND = "Ticket not found";
  static const String PAYMENT_PENDING = "Pending payment";
  static const String PAYMENT_FAILURE = "Failed Transaction";
  static const String PAYMENT_EXPIRED = "Expired payment ";
  static const String BUS_MISMATCH = "Bus mismatch!";
  static const String TRIP_IS_NOT_ONGOING = "This trip is not ongoing";
  static const String TICKET_ALREADY_SCANNED = "Ticket already scanned";
  static const String TRIP_IS_NOT_BOARDING = "Not yet boarding!";

  static const String PASSENGER_PHONE_NUMBER_ALREADY_EXISTS =
      "PASSENGER_PHONE_NUMBER_ALREADY_EXISTS";
  static const String PASSENGER_EMAIL_ALREADY_EXISTS =
      "PASSENGER_EMAIL_ALREADY_EXISTS";
  static const String PASSENGER_SOCIAL_ID_ALREADY_EXISTS =
      "PASSENGER_SOCIAL_ID_ALREADY_EXISTS";
  static const String REFERRAL_CODE_NOT_FOUND = "REFERRAL_CODE_NOT_FOUND";
  static const String PASSENGER_NOT_FOUND = "PASSENGER_NOT_FOUND";
  static const String PASSENGER_HAS_BEEN_SUSPENDED =
      "PASSENGER_HAS_BEEN_SUSPENDED";
  static const String PASSENGER_HAS_BEEN_DELETED = "PASSENGER_HAS_BEEN_DELETED";
  static const String SOCIAL_ID_NOT_FOUND = "SOCIAL_ID_NOT_FOUND";

  static const String ADDRESS_LABEL_EXISTS = "ADDRESS_LABEL_EXISTS";
  static const String ADDRESS_LABEL_NOT_FOUND = "ADDRESS_LABEL_NOT_FOUND";
  static const String STOP_STATION_NOT_FOUND = "STOP_STATION_NOT_FOUND";
  static const String PARCEL_NOT_FOUND = "PARCEL_NOT_FOUND";
  static const String PARCEL_NOT_YET_RECEIVED = "PARCEL_NOT_YET_RECEIVED";
  static const String UNAUTHORIZED_DELIVERY_PERSON =
      "UNAUTHORIZED_DELIVERY_PERSON";
  static const String OTP_NOT_VERIFIED = "OTP_NOT_VERIFIED";
  static const String PARCEL_ALREADY_SCANNED = "PARCEL_ALREADY_SCANNED";
  static const String PARCEL_BOARDING_ALREADY_SCANNED =
      "PARCEL_BOARDING_ALREADY_SCANNED";
  static const String PARCEL_RECEIVING_ALREADY_SCANNED =
      "PARCEL_RECEIVING_ALREADY_SCANNED";
  static const String PARCEL_ALREADY_CLAIMED = "PARCEL_ALREADY_CLAIMED";
  static const String TRIP_MISMATCH = "TRIP_MISMATCH";
  static const String INVALID_FEEDBACK_TYPE = "INVALID_FEEDBACK_TYPE";
  static const String UNAUTHORIZED_NOTIFICATION_ACCESS =
      "UNAUTHORIZED_NOTIFICATION_ACCESS";
  static const String TRIP_NOT_ONGOING = "TRIP_NOT_ONGOING";
  static const String TRIP_CANNOT_BE_TRACKED = "TRIP_CANNOT_BE_TRACKED";
  static const String SOS_LIMIT_EXCEEDED = "SOS_LIMIT_EXCEEDED";
  static const String SOME_SEATS_ALREADY_LOCKED = "SOME_SEATS_ALREADY_LOCKED";
  static const String COUPON_NOT_FOUND = "COUPON_NOT_FOUND";
  static const String MIN_BOOKING_WINDOW_EXCEEDED =
      "MIN_BOOKING_WINDOW_EXCEEDED";
  static const String TRIP_SOURCE_MISMATCH = "TRIP_SOURCE_MISMATCH";
  static const String SEATS_UNAVAILABLE = "SEATS_UNAVAILABLE";
  static const String OTC_PCHANNEL_REQUIRED = "OTC_PCHANNEL_REQUIRED";
  static const String OTC_PAYMENT_FAILURE = "OTC_PAYMENT_FAILURE";
  static const String TRIP_HAS_BEEN_DELETED = "TRIP_HAS_BEEN_DELETED";
  static const String COUPON_NOT_APPLICABLE = "COUPON_NOT_APPLICABLE";
  static const String TRIP_HAS_BEEN_STARTED = "TRIP_HAS_BEEN_STARTED";

  static const String BOOTH_USER_HAS_BEEN_SUSPENDED =
      "BOOTH_USER_HAS_BEEN_SUSPENDED";
  static const String BOOTH_USER_HAS_BEEN_DELETED =
      "BOOTH_USER_HAS_BEEN_DELETED";
  static const String BOOTH_USER_NOT_FOUND = "BOOTH_USER_NOT_FOUND";
  static const String DELIVERY_PERSON_HAS_BEEN_DELETED =
      "DELIVERY_PERSON_HAS_BEEN_DELETED";
  static const String DELIVERY_PERSON_HAS_BEEN_SUSPENDED =
      "DELIVERY_PERSON_HAS_BEEN_SUSPENDED";
  static const String DELIVERY_PERSON_NOT_FOUND = "DELIVERY_PERSON_NOT_FOUND";

  static const String DEFAULT_CODE = "Something went wrong!";

//STRINGS

  //Login Strings
  static const String STAFF_LOGIN = "Staff ID";
  static const String PASSWORD = "Password";
  static const String SIGN_IN = "Sign In";
  static const String BOTTOM_TEXT = "DRIVER | CONDUCTOR | CASHIER";

  //Textfield
  static const String THIS_FIELD_REQUIRED = "This field is required!";

  //Success
  static const String SUCCESS = "This ticket has been successfully scanned.";
}
