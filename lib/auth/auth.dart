import 'dart:convert';
// import 'dart:html';
//import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
//import 'package:helloworld/Layout/About.dart';
import 'package:helloworld/Layout/ScanTicketActivity/ScanTicketView.dart';
// import 'package:helloworld/Layout/home_page.dart';
//import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:toast/toast.dart';
import '../ApiResponse/LoginResponse.dart';
import '../ServerConfig/Constants.dart';
import '../errorCodes/errorCodes.dart';
//import 'package:material_snackbar/material_snackbar.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
// import 'package:flutter/services.dart';
// import 'package:top_snackbar_flutter/tap_bounce_container.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
// import '../value/strings.dart';

Future loginReq(String staffId, String password, BuildContext context) async {
  String fiveStarCashier = "five-star";
  String url = config().loginURL;
  Map body = {"staffId": staffId, "password": password};

  Map<String, String> header = {
    "x-auth-deviceid": "1",
    "x-auth-devicetype": "1"
  };

  Dio dio = new Dio();
  Response<String> response =
      await dio.post(url, data: body, options: Options(headers: header));
  print(url);
  print(header);
  print(body);
  if (response.statusCode == 200) {
    String objText = '${response.data}';

    User user = User.fromJson(jsonDecode(objText));
    print(user.success);

    if (user.success == true) {
      print(user.success);
      print(user.data['token']);
      Map getUser = user.data['user'];
      Map getbusCompanyId = getUser['busCompanyId'];
      Map getPersonalInfo = getUser['personalInfo'];
      String getToken = user.data['token'];
      int getRole = getUser['role'];
      String getFullName = getPersonalInfo['fullName'];
      String getTag = getbusCompanyId['tag'];
      String companyName = getbusCompanyId['name'];
      String getLogo = getbusCompanyId['logo'];

      print(user.data);
      bool isFiveCash = getTag.contains(fiveStarCashier);
      if (isFiveCash) {
        //print(getTag);
        // saveDataToPrefs(getTag);
        saveDataToPrefs(getFullName, companyName, getRole, getLogo, getToken);
         Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ScanTicketView()),
                );
      } else {
        saveDataToPrefs(getFullName, companyName, getRole, getLogo, getToken);
         Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ScanTicketView()),
                );
      }
    } else {
      print(user.success);
      errCode(user.errorCode);
      print(returnString().errStr);
      showTopSnackBar(
        context,
        CustomSnackBar.error(
          icon: const Icon(Icons.warning_amber_outlined, size: 60),
          message: returnString().errStr,
          textStyle: TextStyle(color: Colors.black),
          backgroundColor: Colors.white,
          iconRotationAngle: 0,
        ),
      );
    }
  } else {
    print("Response data: ${response.data}");
  }
}

void saveDataToPrefs(String name, String busCompanyName, int role, String logo,
    String token) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.clear();
  prefs.setString('name', name);
  prefs.setString('busCompanyName', busCompanyName);
  prefs.setInt('role', role);
  prefs.setString('logo', logo);
  prefs.setString('token', token);

  // return prefs.commit();
}
