import 'package:flutter/material.dart';
//import 'package:helloworld/ApiResponse/LoginResponse.dart' as loginresp;
import 'package:helloworld/Layout/LoginActivity/login_page.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey qrKey = GlobalKey();
  var qrText = "";
  QRViewController controller;
  String str = "";
  @override
  Widget build(BuildContext context) {
    // ignore: todo
    // TODO: implement build
    Widget navBarTitle = new Text("Scan Ticket");
   return Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          backgroundColor: Colors.teal,
          title: navBarTitle,
          leading: (IconButton(
              icon: Icon(Icons.info),
              onPressed: () {
                //intent to login screen
              })),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.logout),
                onPressed: () {
                  //intent to login screen
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  );
                }),
          ],
        ),
        body: ListView(
          
          // this will make your body scrollable
          //physics: NeverScrollableScrollPhysics(),
          /// your parameters
          children: <Widget>[
            SizedBox(
              height: 32,
            ),
 
            Container(
                alignment: Alignment.topCenter,
                child: SizedBox(
                  child: Text('Align the QRcode within the Frame to Scan'),
                )),
                 SizedBox(
                    height: 16,
                  ),
            // your widgets,
            // your widget...
 
            Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Container(
                height: 250,
                width: MediaQuery.of(context).size.width,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(25)),
                child: QRView(
                    key: qrKey,
                    overlay: QrScannerOverlayShape(
                        borderRadius: 5,
                        borderColor: Colors.teal,
                        borderLength: 40,
                        cutOutSize: 200),
                    onQRViewCreated: _onQRViewCreate),
              ),
            ),

            Container(
              padding: EdgeInsets.all(12),
              child: Column(
                children: <Widget>[
                 
                  SizedBox(
                    height: 16,
                  ),
                  SizedBox(
                    height: 32,
                    child: Text('or input the ticket number',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        )),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 180,
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Ticket number',

                              isDense: true, // Added this
                            ),
                          ),
                        ),
                        new SizedBox(
                          // width: 200.0,
                          height: 50.0,
                          child: RaisedButton(
                            //shape: RoundedRectangleBorder(
                            //   borderRadius: BorderRadius.circular(25)),
                            color: Colors.teal,
                            child: Text(
                              'CONFIRM',
                              style:
                                  TextStyle(fontSize: 22, color: Colors.white),
                            ),
                            splashColor: Colors.teal,
                            onPressed: () {
                              setState(() {
                                // isLoading = true;
                              });
                              //parseData();
                            },
                          ),
                        ),
                      ]),
                ],
              ),
            ),
            //new Container(
            //  width: 250.0,
            //    child: new TextFormField(
            //      autofocus: false,
            //      decoration: new InputDecoration(
            //       contentPadding: new EdgeInsets.symmetric(
            //           vertical: 10.0, horizontal: 10.0),
            //      // contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
            //     labelText: "Ticket ID",
            //     fillColor: Colors.transparent,
            //     border: new OutlineInputBorder(
            //      borderRadius: new BorderRadius.circular(25.0),
            //     borderSide: new BorderSide(),
            //    ),
            //   ),
            //  )),
          ],
        ));
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreate(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }
}

getParse(BuildContext context) {}
