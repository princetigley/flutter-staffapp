import 'package:flutter/material.dart';
//import 'package:helloworld/ApiResponse/LoginResponse.dart' as loginresp;
import 'package:helloworld/Layout/LoginActivity/login_page.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';
//import 'package:google_fonts/google_fonts.dart';
//import 'package:ticketview/ticketview.dart';

class FxDriver extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<FxDriver> {
  GlobalKey qrKey = GlobalKey();
  var qrText = "";
  QRViewController controller;
  String str = "";
  @override
  Widget build(BuildContext context) {
    // ignore: todo
    // TODO: implement build
    Widget navBarTitle = new Text("Scan Ticket1");
    return Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          backgroundColor: Colors.teal,
          title: navBarTitle,
          leading: (IconButton(
              icon: Icon(Icons.info),
              onPressed: () {
                //intent to login screen
              })),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.logout),
                onPressed: () {
                  //intent to login screen
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  );
                }),
          ],
        ),
        body: ListView(
          // this will make your body scrollable
          //physics: NeverScrollableScrollPhysics(),
          /// your parameters
          children: <Widget>[
            SizedBox(
              height: 32,
            ),

            Container(
                alignment: Alignment.topCenter,
                child: SizedBox(
                  child: Text('Align the QRcode within the Frame to Scan'),
                )),
            SizedBox(
              height: 16,
            ),
            // your widgets,
            // your widget...
            // SizedBox(
            //                  width: 200.0,
            // height: 350.0,
            // child:  Card(elevation: 10,
            //             shape: RoundedRectangleBorder(
            //                 borderRadius: BorderRadius.circular(25)),
            //             child: Container(
            //               height: 350,
            //               width: MediaQuery.of(context).size.width,
            //               decoration:
            //                   BoxDecoration(borderRadius: BorderRadius.circular(25)),
            //               child: QRView(
            //                   key: qrKey,
            //                   overlay: QrScannerOverlayShape(
            //                       borderRadius: 5,
            //                       borderColor: Colors.teal,
            //                       borderLength: 40,
            //                       cutOutSize: 200),
            //                   onQRViewCreated: _onQRViewCreate),
            //             ),),
            //                 ),
            Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Container(
                height: 350,
                width: MediaQuery.of(context).size.width,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(25)),
                child: QRView(
                    key: qrKey,
                    overlay: QrScannerOverlayShape(
                        borderRadius: 5,
                        borderColor: Colors.teal,
                        borderLength: 40,
                        cutOutSize: 200),
                    onQRViewCreated: _onQRViewCreate),
              ),
            ),

            Container(
              padding: EdgeInsets.all(12),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 16,
                  ),
                  SizedBox(
                    height: 32,
                    child: Text('or input the ticket number',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        )),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 180,
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Ticket number',

                              isDense: true, // Added this
                            ),
                          ),
                        ),
                        new SizedBox(
                          // width: 200.0,
                          height: 50.0,
                          child: RaisedButton(
                            //shape: RoundedRectangleBorder(
                            //   borderRadius: BorderRadius.circular(25)),
                            color: Colors.teal,
                            child: Text(
                              'CONFIRM',
                              style:
                                  TextStyle(fontSize: 22, color: Colors.white),
                            ),
                            splashColor: Colors.teal,
                            onPressed: () {
                              setState(() {
                                // isLoading = true;
                              });
                              //parseData();
                            },
                          ),
                        ),
                      ]),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void _onQRViewCreate(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }
}

getParse(BuildContext context) {}
