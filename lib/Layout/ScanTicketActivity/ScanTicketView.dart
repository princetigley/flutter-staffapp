import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:helloworld/ApiResponse/LoginResponse.dart';
import 'package:helloworld/Layout/Pages/About.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';

import 'package:helloworld/Layout/LoginActivity/login_page.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../ServerConfig/Constants.dart';
import 'package:helloworld/assets/strings/strings.dart';
import '../../errorCodes/errorCodes.dart';

class ScanTicketView extends StatefulWidget {
  @override
  _ScanTicketViewState createState() => _ScanTicketViewState();
}

//bool _showTicketView = true;
class _ScanTicketViewState extends State<ScanTicketView> {
  TextEditingController ticketController = new TextEditingController();
  String _ticketHint = "Ticket number";
  MediaQueryData queryData;

  GlobalKey qrKey = GlobalKey();
  String qrText = "";
  QRViewController controller;

  String str = "";

  @override
  void initState() {
    super.initState();
    fetchData123();
    _session();
        // controller.flipCamera();
        // controller.flipCamera();
        // startCamera();
        // QRView(key: qrKey, onQRViewCreated: (QRViewController controller) {  },);
        // WidgetsBinding.instance.addPostFrameCallback((_) => startCamera());
      }
    
      Timer timer, timer2;
      String scanTicketUrl;
      String tokenCreds;
      String staffRole;
      // ignore: missing_return
      Future<String> fetchData123() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String token = prefs.getString('token');
        int role = prefs.getInt('role');
        setState(() => tokenCreds = token);
        if (role == 4) {
          setState(() => staffRole = "Cashier");
          setState(() => scanTicketUrl = config().checkTicket);
        } else if (role == 1) {
          setState(() => staffRole = "Driver");
          setState(() => scanTicketUrl = config().checkTicket);
        } else if (role == 2) {
          setState(() => staffRole = "Conductor");
          setState(() => scanTicketUrl = config().checkTicket);
        } else if (role == 3) {
          setState(() => staffRole = "FX Driver");
          setState(() => scanTicketUrl = config().fxDriver);
        } else {
          setState(() => staffRole = role.toString());
          setState(() => scanTicketUrl = config().checkTicket);
        }
      }
    
      Future scanCodePOST(String token, String scanCode) async {
        controller.pauseCamera();
        Map body = {"scanCode": scanCode};
    
        Map<String, String> header = {
          "x-auth-deviceid": "1",
          "x-auth-devicetype": "1",
          "x-auth-token": token,
        };
        // print("HEADER :" + header.toString());
        String objText;
        User user;
        Dio dio = new Dio();
        // try {
        Response<String> response = await dio.post(scanTicketUrl,
            data: body, options: Options(headers: header));
        // print("test" + objText);
        if (response.statusCode == 200) {
          objText = '${response.data}';
          user = User.fromJson(jsonDecode(objText));
          print("test" + objText);
          if (user.success == true) {
            _successDialog(Strings.SUCCESS);
          } else if (user.success == false) {
            errCode(user.errorCode);
            _responseDialog(returnString().errStr);
          }
        } else {
          _responseDialog(Strings.DEFAULT_CODE);
        }
        // } on Exception catch (e) {
    
        //   return null;
        // }
        // _showDialogFailed();
    
        //controller.pauseCamera();
      }
    
      @override
      Widget build(BuildContext context) {
        // ignore: todo
        // TODO: implement build
    
        Widget navBarTitle = new Text(staffRole);
        return Scaffold(
            appBar: new AppBar(
              centerTitle: true,
              backgroundColor: Colors.teal,
              title: navBarTitle,
              leading: (IconButton(
                  icon: Icon(Icons.info),
                  onPressed: () {
                    //intent to login screen
                    controller.pauseCamera();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AboutPage()),
                    );
                  })),
              actions: <Widget>[
                IconButton(
                    icon: Icon(Icons.logout),
                    onPressed: () {
                      //intent to login screen
                      _logoutDialogBox();
                    }),
              ],
            ),
            backgroundColor: Colors.white,
            body: ListView(
              // this will make your body scrollable
              //physics: NeverScrollableScrollPhysics(),
              /// your parameters
              children: <Widget>[
                SizedBox(
                  height: 32,
                ),
    
                Container(
                    alignment: Alignment.topCenter,
                    child: SizedBox(
                      child: Text('Align the QRcode within the Frame to Scan'),
                    )),
                SizedBox(
                  height: 16,
                ),
                // your widgets,
                // your widget...
                //  Expanded(flex: , child: _buildQrView(context)),
                // SizedBox(
                //   height: MediaQuery.of(context).size.height * .40,
                //   child: _buildQrView(context),
                // ),
                Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  child: Container(
                    height: 250,
                    width: MediaQuery.of(context).size.width,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(25)),
                    child: QRView(
                        key: qrKey,
                        overlay: QrScannerOverlayShape(
                            borderRadius: 5,
                            borderColor: Colors.teal,
                            borderLength: 40,
                            cutOutSize: 200),
                        onQRViewCreated: _onQRViewCreated),
                  ),
                ),
    
                Container(
                  padding: EdgeInsets.all(12),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 16,
                      ),
                      SizedBox(
                        height: 32,
                        child: Text('or input the ticket number',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                            )),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 180,
                              child: TextField(
                                controller: ticketController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Ticket number',
                                  hintText: _ticketHint,
                                  isDense: true, // Added this
                                ),
                              ),
                            ),
                            new SizedBox(
                              // width: 200.0,
                              height: 50.0,
                              child: RaisedButton(
                                color: Colors.teal,
                                child: Text(
                                  'CONFIRM',
                                  style:
                                      TextStyle(fontSize: 22, color: Colors.white),
                                ),
                                splashColor: Colors.teal,
                                onPressed: () {
                                  String ticketNoTxt = ticketController.text;
                                  if (tokenCreds.isEmpty) {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => LoginPage()));
                                  } else if (ticketNoTxt.isEmpty) {
                                    setState(() {
                                      _ticketHint = Strings.THIS_FIELD_REQUIRED;
                                    });
                                  } else {
                                    scanCodePOST(
                                        tokenCreds, ticketController.toString());
                                  }
                                },
                              ),
                            ),
                          ]),
                    ],
                  ),
                ),
              ],
            ));
      }
    
      // ignore: unused_element
      void _showDialogFailed() {
        controller.pauseCamera();
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 10,
                ),
                child: IntrinsicWidth(
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Icon(Icons.warning),
                        Text(
                          "",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: "Roboto",
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: Text("body",
                              style: TextStyle(
                                fontFamily: "Roboto",
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30)),
                                color: Colors.teal,
                                child: Text(
                                  'Resume Scanning',
                                  style:
                                      TextStyle(fontSize: 22, color: Colors.white),
                                ),
                                splashColor: Colors.teal,
                                onPressed: () {
                                  setState(() {
                                    // isLoading = true;
                                  });
                                  //parseData();
                                  Navigator.of(context).pop();
                                  controller.resumeCamera();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      }
    
      // ignore: missing_return
      Future<String> _responseDialog(String code) {
        controller.pauseCamera();
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 10,
                ),
                child: IntrinsicWidth(
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Icon(Icons.warning),
                        Text(
                          "",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: "Roboto",
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: Text(code,
                              style: TextStyle(
                                fontFamily: "Roboto",
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30)),
                                color: Colors.teal,
                                child: Text(
                                  'Resume Scanning',
                                  style:
                                      TextStyle(fontSize: 22, color: Colors.white),
                                ),
                                splashColor: Colors.teal,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  controller.resumeCamera();
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => LoginPage()));
    
                                  //controller.resumeCamera();
                                },
                              ),
                            ),
                            // Align(
                            //   alignment: Alignment.center,
                            //   child: RaisedButton(
                            //     shape: RoundedRectangleBorder(
                            //         side: BorderSide(
                            //           color: Colors.teal,
                            //         ),
                            //         borderRadius: BorderRadius.circular(30)),
                            //     color: Colors.white,
                            //     child: Text(
                            //       'NO',
                            //       style:
                            //           TextStyle(fontSize: 22, color: Colors.teal),
                            //     ),
                            //     splashColor: Colors.teal,
                            //     onPressed: () {
                            //       setState(() {
                            //         // isLoading = true;
                            //       });
    
                            //       Navigator.of(context).pop();
                            //       controller.resumeCamera();
                            //     },
                            //   ),
                            // ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      }
    
    // ignore: missing_return
      Future<String> _successDialog(String code) {
        controller.pauseCamera();
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 10,
                ),
                child: IntrinsicWidth(
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Icon(Icons.check),
                        Text(
                          "Congratualation!",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: "Roboto",
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: Text(code,
                              style: TextStyle(
                                fontFamily: "Roboto",
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30)),
                                color: Colors.teal,
                                child: Text(
                                  'Resume Scanning',
                                  style:
                                      TextStyle(fontSize: 22, color: Colors.white),
                                ),
                                splashColor: Colors.teal,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  controller.resumeCamera();
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => LoginPage()));
    
                                  //controller.resumeCamera();
                                },
                              ),
                            ),
                            // Align(
                            //   alignment: Alignment.center,
                            //   child: RaisedButton(
                            //     shape: RoundedRectangleBorder(
                            //         side: BorderSide(
                            //           color: Colors.teal,
                            //         ),
                            //         borderRadius: BorderRadius.circular(30)),
                            //     color: Colors.white,
                            //     child: Text(
                            //       'NO',
                            //       style:
                            //           TextStyle(fontSize: 22, color: Colors.teal),
                            //     ),
                            //     splashColor: Colors.teal,
                            //     onPressed: () {
                            //       setState(() {
                            //         // isLoading = true;
                            //       });
    
                            //       Navigator.of(context).pop();
                            //       controller.resumeCamera();
                            //     },
                            //   ),
                            // ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      }
    
      void _logoutDialogBox() {
        controller.pauseCamera();
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 10,
                ),
                child: IntrinsicWidth(
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                        ),
                        Icon(Icons.warning),
                        Text(
                          "LOGOUT",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: "Roboto",
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: Text("Are you sure you want to logout?",
                              style: TextStyle(
                                fontFamily: "Roboto",
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30)),
                                color: Colors.teal,
                                child: Text(
                                  'YES',
                                  style:
                                      TextStyle(fontSize: 22, color: Colors.white),
                                ),
                                splashColor: Colors.teal,
                                onPressed: () {
                                  _clearSharedPref();
                                  Navigator.of(context).pop();
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LoginPage()));
    
                                  controller.pauseCamera();
                                },
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                      color: Colors.teal,
                                    ),
                                    borderRadius: BorderRadius.circular(30)),
                                color: Colors.white,
                                child: Text(
                                  'NO',
                                  style:
                                      TextStyle(fontSize: 22, color: Colors.teal),
                                ),
                                splashColor: Colors.teal,
                                onPressed: () {
                                  setState(() {
                                    // isLoading = true;
                                  });
    
                                  Navigator.of(context).pop();
                                  controller.resumeCamera();
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      }
    
      // ignore: unused_element
      Future<void> _showMyDialog() async {
        return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Scan Failed'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text('This is a demo alert dialog.'),
                    Text('Would you like to approve of this message?'),
                  ],
                ),
              ),
              actions: <Widget>[
                Container(
                    alignment: Alignment.center,
                    //padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    child: SizedBox(
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        color: Colors.teal,
                        child: Text(
                          'CONFIRM',
                          style: TextStyle(fontSize: 22, color: Colors.white),
                        ),
                        splashColor: Colors.teal,
                        onPressed: () {
                          setState(() {
                            // isLoading = true;
                          });
                          // scanCodePOST(tokenCreds, scanCode)
                          Navigator.of(context).pop();
                        },
                      ),
                    )),
              ],
            );
          },
        );
      }
    
      // ignore: missing_return
      Future<bool> scanTest(String textString) {
        // if (textString.isEmpty) {
        //   // controller.pauseCamera();
        //   controller.resumeCamera();
        // }
    
        _ticketHint = "";
        print(textString);
        scanCodePOST(tokenCreds, textString);
      }
    
      // Widget _buildQrView(BuildContext context) {
      //   // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
      //   var scanArea = (MediaQuery.of(context).size.width < 400 ||
      //           MediaQuery.of(context).size.height < 400)
      //       ? 150.0
      //       : 300.0;
      //   // To ensure the Scanner view is properly sizes after rotation
      //   // we need to listen for Flutter SizeChanged notification and update controller
      //   return QRView(
      //     key: qrKey,
      //     onQRViewCreated: _onQRViewCreated,
      //     overlay: QrScannerOverlayShape(
      //         borderColor: Colors.teal,
      //         borderRadius: 10,
      //         borderLength: 30,
      //         borderWidth: 10,
      //         cutOutSize: scanArea),
      //   );
      // }
    
      void _onQRViewCreated(QRViewController controller) {
        setState(() {
          this.controller = controller;
        });
        controller.scannedDataStream.listen((scanData) {
          setState(() {
            qrText = scanData;
            scanTest(qrText);
          });
        });
      }
    
      @override
      void dispose() {
        controller?.dispose();
        super.dispose();
      }
    
      void startCamera() {
        QRViewController controllerStart;
        controllerStart.resumeCamera();
      }
    
      void _clearSharedPref() async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.clear();
      }
    
      // ignore: unused_element
      void _session() {
        if(tokenCreds==null){
            Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
        }else{
          
        }
      }
}
