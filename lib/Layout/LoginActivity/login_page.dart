// import 'dart:convert';
// import 'dart:html';
//import 'dart:io';
// import 'package:dio/dio.dart';
//import 'package:helloworld/Layout/About.dart';
// import 'package:helloworld/Layout/ScanTicketView.dart';
// import 'package:helloworld/Layout/home_page.dart';
//import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:toast/toast.dart';
// import '../ApiResponse/LoginResponse.dart';
// import '../ServerConfig/Constants.dart';
// import '../errorCodes/errorCodes.dart';
//import 'package:material_snackbar/material_snackbar.dart';
// import 'package:top_snackbar_flutter/custom_snack_bar.dart';
// import 'package:top_snackbar_flutter/tap_bounce_container.dart';
// import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:helloworld/assets/strings/strings.dart';
import '../../auth/auth.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController staffidController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

//Login
  bool isLoading = false;
  bool _isHidden = true;
//Globals
  final String test123 = "abdcd";
  String imgUrl =
      "https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5cfea91f142c50000a3288e7%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D788%26cropX2%3D3590%26cropY1%3D305%26cropY2%3D3105";

  @override
  Widget build(BuildContext context) {
    // final _width = MediaQuery.of(context).size.width;
    // final _height = MediaQuery.of(context).size.height;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
      backgroundColor: Colors.teal[100],
      resizeToAvoidBottomPadding: false,
      body: Center(
        child: Container(
          // decoration: new BoxDecoration(
          //   image: new DecorationImage(
          //     image: new AssetImage('images/bg.jpg'),
          //     fit: BoxFit.fitWidth,
          //     alignment: Alignment(0, 1.2),
          //   ),
          // ),
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Container(
              //     child: new CircleAvatar(
              //         radius: _width < _height ? _width / 4 : _height / 4,
              //         backgroundImage: NetworkImage(imgUrl),
              //         //child: Image.asset('images/rene.jpg'),
              //         foregroundColor: Colors.white,
              //         backgroundColor: Colors.teal),
              //     padding: const EdgeInsets.all(2.0), // borde width
              //     decoration: new BoxDecoration(
              //       color: const Color(0xFFFFFFFF), // border color
              //       shape: BoxShape.circle,
              //     )),
              // new Padding(
              //   padding: new EdgeInsets.only(
              //       top: _height / 30,
              //       bottom: _height / 30,
              //       left: _width / 8,
              //       right: _width / 8),
              //   child: Image.asset('images/movonstaff.png'),
              //   // style: new TextStyle(fontWeight: FontWeight.normal, fontSize: _width/25,color: Colors.white),textAlign: TextAlign.center,)
              // ),
              // Text(
              //   'STAFF LOGIN',
              //   style: TextStyle(
              //       shadows: [
              //         Shadow(
              //           offset: Offset(0.0, 5.0),
              //           blurRadius: 10.0,
              //           color: Colors.grey,
              //         ),
              //       ],
              //       fontSize: 32,
              //       color: Colors.teal,
              //       fontWeight: FontWeight.bold),
              // ),
              SizedBox(
                height: 20,
              ),
              Card(
                elevation: 10,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: Container(
                  height: 230,
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(20)),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: TextField(
                          controller: staffidController,
                          decoration: InputDecoration(
                              labelText: Strings.STAFF_LOGIN,
                              hintText: Strings.STAFF_LOGIN),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: TextField(
                          controller: passwordController,
                          obscureText: _isHidden,
                          decoration: InputDecoration(
                              labelText: Strings.PASSWORD,
                              hintText: Strings.PASSWORD,
                              suffix: InkWell(
                                onTap: _togglePasswordView,
                                child: Icon(
                                  _isHidden
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                                ),
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 60,
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  color: Colors.teal,
                  child: Text(
                    Strings.SIGN_IN,
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  ),
                  onPressed: staffidController.text == "" ||
                          passwordController.text == ""
                      ? null
                      : () {
                          setState(() {
                            isLoading = true;
                          });
                          //parseData();

                          loginReq(staffidController.text,
                              passwordController.text, context);
                        },
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Text(Strings.BOTTOM_TEXT),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }
}

// gotoSecondActivity(BuildContext context) {
//   Navigator.push(
//     context,
//     MaterialPageRoute(builder: (context) => HomePage()),
//   );
// }

// Future loginRequest(
//     String staffId, String password, BuildContext context) async {
//   String fiveStarCashier = "five-star";
//   String url = config().loginURL;
//   Map body = {"staffId": staffId, "password": password};

//   Map<String, String> header = {
//     "x-auth-deviceid": "1",
//     "x-auth-devicetype": "1"
//   };

//   Dio dio = new Dio();
//   Response<String> response =
//       await dio.post(url, data: body, options: Options(headers: header));
//   if (response.statusCode == 200) {
//     String objText = '${response.data}';

//     User user = User.fromJson(jsonDecode(objText));
//     print(user.success);

//     if (user.success == true) {
//       print(user.success);
//       print(user.data['token']);
//       Map getUser = user.data['user'];
//       Map getbusCompanyId = getUser['busCompanyId'];
//       Map getPersonalInfo = getUser['personalInfo'];
//       String getToken = user.data['token'];
//       int getRole = getUser['role'];
//       String getFullName = getPersonalInfo['fullName'];
//       String getTag = getbusCompanyId['tag'];
//       String companyName = getbusCompanyId['name'];
//       String getLogo = getbusCompanyId['logo'];

//       print(getTag);
//       bool isFiveCash = getTag.contains(fiveStarCashier);
//       if (isFiveCash) {
//         //print(getTag);
//         // saveDataToPrefs(getTag);
//         saveDataToPrefs(getFullName, companyName, getRole, getLogo, getToken);
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(
//                 builder: (BuildContext context) => ScanTicketView()),
//             (Route<dynamic> route) => false);
//       } else {
//         saveDataToPrefs(getFullName, companyName, getRole, getLogo, getToken);
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(
//                 builder: (BuildContext context) => ScanTicketView()),
//             (Route<dynamic> route) => false);
//       }
//     } else {
//       print(user.success);
//       errCode(user.errorCode);
//       print(returnString().errStr);
//       // showToast("test",context,duration: 10,gravity: );
//       showTopSnackBar(
//         context,
//         CustomSnackBar.error(
//           icon: const Icon(Icons.warning_amber_outlined, size: 60),
//           message: returnString().errStr,
//           textStyle: TextStyle(color: Colors.black),
//           backgroundColor: Colors.white,
//           iconRotationAngle: 0,
//         ),
//       );
//     }
//   } else {
//     print("Response data: ${response.data}");
//   }
// }

// void showToast(String msg, BuildContext context, {int duration, int gravity}) {
//   Toast.show(msg, context, duration: duration, gravity: gravity);
// }

// void saveDataToPrefs(String name, String busCompanyName, int role, String logo,
//     String token) async {
//   SharedPreferences prefs = await SharedPreferences.getInstance();
//   prefs.clear();
//   prefs.setString('name', name);
//   prefs.setString('busCompanyName', busCompanyName);
//   prefs.setInt('role', role);
//   prefs.setString('logo', logo);
//   prefs.setString('token', token);
//   // return prefs.commit();
// }
