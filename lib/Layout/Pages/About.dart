import 'package:flutter/material.dart';
import 'package:helloworld/Layout/ScanTicketActivity/ScanTicketView.dart';
//import 'package:helloworld/Layout/login_page.dart';
//import 'package:helloworld/ServerConfig/Constants.dart';
//import 'package:helloworld/SharedPreference/Fetch.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:helloworld/ApiResponse/LoginResponse.dart' as loginresp;
//import 'package:helloworld/login_page.dart';
// import 'package:qr_code_scanner/qr_code_scanner.dart';
//import 'package:qr_code_scanner/qr_scanner_overlay_shape.dart';
// import '../ApiResponse/LoginResponse.dart';
import 'package:package_info/package_info.dart';
import 'package:helloworld/Layout/LoginActivity/login_page.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    //: '',
    buildNumber: 'Unknown',
    //: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    fetchData123();
    _session();
    //datafetchData123();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  String fullname = "";
  String busCompanyName = "";
  String staffRole;
  String imgUrl;
  String tokenCreds;
  // ignore: missing_return
  Future<String> fetchData123() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String name = prefs.getString('name');
    String busCname = prefs.getString('busCompanyName');
    String token = prefs.getString('token');
    int role = prefs.getInt('role');

    String networkImg = prefs.getString('logo');
    setState(() => fullname = name);
    setState(() => busCompanyName = busCname);
    if (role == 4) {
      setState(() => staffRole = "Cashier");
    } else if (role == 1) {
      setState(() => staffRole = "Driver");
    } else if (role == 2) {
      setState(() => staffRole = "Conductor");
    } else if (role == 3) {
      setState(() => staffRole = "FX Driver");
      setState(()=>tokenCreds=token);
    } else {
      setState(() => staffRole = role.toString());
    }
    if (networkImg == null) {
      setState(() => imgUrl =
          "https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5cfea91f142c50000a3288e7%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D788%26cropX2%3D3590%26cropY1%3D305%26cropY2%3D3105");
    } else {
      setState(() => imgUrl = networkImg);
    }
  }

  // ignore: unused_element
  Widget _infoTile(String title, String subtitle) {
    return ListTile(
      title: Text(title),
      subtitle: Text(subtitle ?? 'Not set'),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;
    // ignore: todo
    // TODO: implement build
    Widget navBarTitle = new Text("About");
    return Scaffold(
        backgroundColor: Colors.teal[200].withOpacity(0.9),
        appBar: new AppBar(
          centerTitle: true,
          backgroundColor: Colors.teal,
          title: navBarTitle,
          leading: (IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                //intent to login screen
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ScanTicketView()),
                );
              })),
          // actions: <Widget>[
          //   IconButton(
          //       icon: Icon(Icons.logout),
          //       onPressed: () {
          //         //intent to login screen

          //       }),
          // ],
        ),
        body: new SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              new SizedBox(
                height: _height / 12,
              ),
              new Container(
                  child: new CircleAvatar(
                      radius: _width < _height ? _width / 4 : _height / 4,
                      backgroundImage: NetworkImage(imgUrl),
                      //child: Image.asset('images/rene.jpg'),
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.teal),
                  padding: const EdgeInsets.all(2.0), // borde width
                  decoration: new BoxDecoration(
                    color: const Color(0xFFFFFFFF), // border color
                    shape: BoxShape.circle,
                  )),
              // new CircleAvatar(
              //   radius: _width < _height ? _width / 4 : _height / 4,
              //   backgroundImage: NetworkImage(imgUrl),
              // ),
              new SizedBox(
                height: _height / 25.0,
              ),
              new Text(
                fullname,
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: _width / 15,
                    color: Colors.white),
              ),
              new Padding(
                padding: new EdgeInsets.only(
                    top: _height / 30, left: _width / 8, right: _width / 8),
                // child:new Text('Role: FX DRIVER',
                // style: new TextStyle(fontWeight: FontWeight.normal, fontSize: _width/25,color: Colors.white),textAlign: TextAlign.center,)
              ),
              new Divider(
                height: _height / 30,
                color: Colors.white,
              ),
              new Row(
                children: <Widget>[
                  rowCell(busCompanyName, 'Bus Company'),
                  rowCell(
                      _packageInfo.version +
                          ".(" +
                          _packageInfo.buildNumber +
                          ")",
                      'Staging'),
                  rowCell(staffRole.toString(), 'Role'),
                  //rowCell('Five Star', 'Five Star')
                ],
              ),
              new Divider(height: _height / 30, color: Colors.white),
              // new Padding(
              //   padding: new EdgeInsets.only(
              //       top: _height / 30, left: _width / 8, right: _width / 8),
              //   child: Image.asset('images/movonwhite.png'),
              //   // style: new TextStyle(fontWeight: FontWeight.normal, fontSize: _width/25,color: Colors.white),textAlign: TextAlign.center,)
              // ),
              // new Row(
              //   children: <Widget>[
              //     rowCell(_packageInfo.version+".("+_packageInfo.buildNumber+")", 'Staging'),

              //     //rowCell('Five Star', 'Five Star')
              //   ],
              // ),
              //     _infoTile('App name', _packageInfo.appName),
              // _infoTile('Package name', _packageInfo.packageName),
              // _infoTile('App version', _packageInfo.version),
              // _infoTile('Build number', _packageInfo.buildNumber),
              //_infoTile('Build number', _packageInfo.),
              // _infoTile('Build number', _packageInfo.versionCode),
            ],
          ),
        ));
  }

  Widget rowCell(String count, String type) => new Expanded(
          child: new Column(
        children: <Widget>[
          new Text('$count',
              style: new TextStyle(color: Colors.white),
              textAlign: TextAlign.center),
          new Text(
            type,
            style: new TextStyle(
                color: Colors.white, fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,
          )
        ],
      ));

  void _session() {
    if (tokenCreds == null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    } else {}
  }
}
