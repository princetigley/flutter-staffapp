import '../assets/strings/strings.dart';

String reString;
Future errCode(int errCode) async {
  switch (errCode) {
    case 400:
      return reString = Strings.API_VALIDATION_ERROR;
    case 401:
      return reString = Strings.UPLOAD_ERROR;
    case 403:
      return reString = Strings.UNKNOWN_ERROR;
    case 404:
      return reString = Strings.ADMIN_NO_ACCESS;
    case 501:
      return reString = Strings.INVALID_FILE_TYPE;
    case 502:
      return reString = Strings.NO_USER_ACCESS;
    case 1000:
      return reString = Strings.SESSION_NOT_FOUND;
    case 1001:
      return reString = Strings.OTP_INVALID;
    case 1002:
      return reString = Strings.OTP_TIMEDOUT;
    case 1003:
      return reString = Strings.PASSWORD_MISMATCH;
    case 1004:
      return reString = Strings.BUS_NOT_ACTIVE;
    case 1005:
      return reString = Strings.BUS_NOT_FOUND;
    case 1006:
      return reString = Strings.BUS_HAS_BEEN_DELETED;
    case 2000:
      return reString = Strings.CODE_NOT_FOUND;
    case 2001:
      return reString = Strings.TICKET_NOT_FOUND;
    case 2002:
      return reString = Strings.PAYMENT_PENDING;
    case 2003:
      return reString = Strings.PAYMENT_FAILURE;
    case 2004:
      return reString = Strings.PAYMENT_EXPIRED;
    case 2005:
      return reString = Strings.BUS_MISMATCH;
    case 2006:
      return reString = Strings.TRIP_IS_NOT_ONGOING;
    case 2007:
      return reString = Strings.TICKET_ALREADY_SCANNED;
    case 2008:
      return reString = Strings.TRIP_IS_NOT_BOARDING;
    case 2500:
      return reString = Strings.PASSENGER_PHONE_NUMBER_ALREADY_EXISTS;
    case 2501:
      return reString = Strings.PASSENGER_EMAIL_ALREADY_EXISTS;
    case 2502:
      return reString = Strings.PASSENGER_SOCIAL_ID_ALREADY_EXISTS;
    case 2503:
      return reString = Strings.REFERRAL_CODE_NOT_FOUND;
    case 2504:
      return reString = Strings.PASSENGER_NOT_FOUND;
    case 2505:
      return reString = Strings.PASSENGER_HAS_BEEN_SUSPENDED;
    case 2506:
      return reString = Strings.PASSENGER_HAS_BEEN_DELETED;
    case 2507:
      return reString = Strings.SOCIAL_ID_NOT_FOUND;
    case 2600:
      return reString = Strings.STAFF_NOT_FOUND;
    case 3001:
      return reString = Strings.ADDRESS_LABEL_EXISTS;
    case 3002:
      return reString = Strings.ADDRESS_LABEL_NOT_FOUND;
    case 3500:
      return reString = Strings.STOP_STATION_NOT_FOUND;
    case 4000:
      return reString = Strings.PARCEL_NOT_FOUND;
    case 4001:
      return reString = Strings.PARCEL_NOT_YET_RECEIVED;
    case 4002:
      return reString = Strings.UNAUTHORIZED_DELIVERY_PERSON;
    case 4003:
      return reString = Strings.OTP_NOT_VERIFIED;
    case 4004:
      return reString = Strings.PARCEL_ALREADY_SCANNED;
    case 4005:
      return reString = Strings.PARCEL_BOARDING_ALREADY_SCANNED;
    case 4006:
      return reString = Strings.PARCEL_RECEIVING_ALREADY_SCANNED;
    case 4007:
      return reString = Strings.PARCEL_ALREADY_CLAIMED;
    case 4008:
      return reString = Strings.TRIP_MISMATCH;
    case 4500:
      return reString = Strings.INVALID_FEEDBACK_TYPE;
    case 5000:
      return reString = Strings.UNAUTHORIZED_NOTIFICATION_ACCESS;
    case 5500:
      return reString = Strings.TRIP_NOT_ONGOING;
    case 5501:
      return reString = Strings.TRIP_CANNOT_BE_TRACKED;
    case 5502:
      return reString = Strings.SOS_LIMIT_EXCEEDED;
    case 6000:
      return reString = Strings.SOME_SEATS_ALREADY_LOCKED;
    case 6001:
      return reString = Strings.COUPON_NOT_FOUND;
    case 6002:
      return reString = Strings.MIN_BOOKING_WINDOW_EXCEEDED;
    case 6003:
      return reString = Strings.TRIP_SOURCE_MISMATCH;
    case 6004:
      return reString = Strings.SEATS_UNAVAILABLE;
    case 6005:
      return reString = Strings.OTC_PCHANNEL_REQUIRED;
    case 6006:
      return reString = Strings.OTC_PAYMENT_FAILURE;
    case 6007:
      return reString = Strings.TRIP_HAS_BEEN_DELETED;
    case 6008:
      return reString = Strings.COUPON_NOT_APPLICABLE;
    case 6009:
      return reString = Strings.TRIP_HAS_BEEN_STARTED;
    case 6500:
      return reString = Strings.BOOTH_USER_HAS_BEEN_SUSPENDED;
    case 6501:
      return reString = Strings.BOOTH_USER_HAS_BEEN_DELETED;
    case 6502:
      return reString = Strings.BOOTH_USER_NOT_FOUND;
    case 7000:
      return reString = Strings.DELIVERY_PERSON_HAS_BEEN_DELETED;
    case 7001:
      return reString = Strings.DELIVERY_PERSON_HAS_BEEN_SUSPENDED;
    case 7002:
      return reString = Strings.DELIVERY_PERSON_NOT_FOUND;
  }
  return reString = Strings.DEFAULT_CODE;
}


// ignore: camel_case_types
class returnString {
  String errStr = reString;
}
