//import 'package:helloworld/login_page.dart';

// ignore: camel_case_types
class API_PATH {
  //Login
  String apiLogin = 'staff/auth/personal/login';
  String apiTicketValidate = 'account/staff/ticket/scan';
  String fxDriver = 'account/staff/ticket/p2p-conductor-scan';
//www.cargomovon.com/server/api/v1/staff/auth/personal/login

}

// ignore: camel_case_types
class SERVER_PATH {
  String staging = 'www.cargomovon.com/server/api/v1/';
  String prod = 'www.movon.com.ph/server/api/v1/';

//www.cargomovon.com/server/api/v1/staff/auth/personal/login

}

// ignore: camel_case_types
class SERVER_SELECTION {
  String current = SERVER_PATH().staging;
}

// ignore: camel_case_types
class config {
  String loginURL =
      'https://' + SERVER_SELECTION().current + API_PATH().apiLogin;
  String checkTicket =
      'https://' + SERVER_SELECTION().current + API_PATH().apiTicketValidate;
  String fxDriver =
      "https://www.cargomovon.com/server/api/v1/account/staff/ticket/p2p-conductor-scan";
  // 'https://' + SERVER_SELECTION().current + API_PATH().fxDriver;
}
