class User {
  bool success;
  int errorCode;
  final Map data;

  User(this.success, this.errorCode, this.data);

  factory User.fromJson(dynamic json) {
    return User(
        json['success'] as bool, json['errorCode'] as int, json['data'] as Map);
  }

  @override
  String toString() {
    return '{ ${this.success}, ${this.errorCode}, ${this.data} }';
  }
}

